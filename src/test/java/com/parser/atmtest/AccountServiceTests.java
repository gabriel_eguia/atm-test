package com.parser.atmtest;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.parser.atmtest.exception.InsufficientFundsException;
import com.parser.atmtest.service.AccountService;
import com.parser.atmtest.service.impl.AccountServiceImpl;

@DisplayName("Accounts Tests")
public class AccountServiceTests {
	
	private AccountService accountService;
	
	@BeforeEach
	private void setUp() {
		this.accountService = new AccountServiceImpl();
	}

	@Test
	@DisplayName("Test Accounts Balances")
	public void testBalance() {
		
		Assertions.assertEquals(this.accountService.checkBalance("01001"), BigDecimal.valueOf(2738.59));
		Assertions.assertNotEquals(this.accountService.checkBalance("01003"), BigDecimal.valueOf(1.00));
	}
	
	@Test
	@DisplayName("Test Accounts Withdrawal")
	public void testWithdrawal() {
		
		Assertions.assertEquals(this.accountService.withdraw("01001", BigDecimal.valueOf(1000.00)), BigDecimal.valueOf(1738.59));
		Assertions.assertNotEquals(this.accountService.withdraw("01002", BigDecimal.valueOf(20.00)), BigDecimal.valueOf(5.00));
		
		Assertions.assertThrows(InsufficientFundsException.class, () -> this.accountService.withdraw("01003", BigDecimal.valueOf(1.00)));
	}
}
