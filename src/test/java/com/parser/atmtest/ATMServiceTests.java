package com.parser.atmtest;

import java.math.BigDecimal;
import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.parser.atmtest.exception.InsufficientFundsException;
import com.parser.atmtest.exception.InvalidWithdrawalAmountException;
import com.parser.atmtest.exception.UnsopportedDenominationException;
import com.parser.atmtest.service.ATMService;
import com.parser.atmtest.service.impl.ATMServiceImpl;
import com.parser.atmtest.service.impl.AccountServiceImpl;

@DisplayName("ATM Tests")
public class ATMServiceTests {
	
	private ATMService atmService;
	
	@BeforeEach
	private void setUp() {
		this.atmService = new ATMServiceImpl(new AccountServiceImpl());
	}

	@Test
	@DisplayName("Test Accounts Balances")
	public void testBalance() {
		
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2738.59));
		Assertions.assertNotEquals(this.atmService.checkBalance("01003"), "GBP $ " + BigDecimal.valueOf(1.00));
	}
	
	@Test
	@DisplayName("Test ATM Withdrawal")
	public void testWithdrawal() {
		
		this.replenshATM();
		
		Assertions.assertThrows(InvalidWithdrawalAmountException.class, () -> this.atmService.withdraw("01001", 1));
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2738.59));
		Assertions.assertThrows(InvalidWithdrawalAmountException.class, () -> this.atmService.withdraw("01001", 251));
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2738.59));
		Assertions.assertThrows(InvalidWithdrawalAmountException.class, () -> this.atmService.withdraw("01001", 21));
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2738.59));
		
		Assertions.assertThrows(InsufficientFundsException.class, () -> this.atmService.withdraw("01003", 20));
		
		HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
		result.put(50, 2);
		
		Assertions.assertNotEquals(this.atmService.withdraw("01001", 100), result);
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2638.59));
		
		result.put(50, 1);
		result.put(20, 2);
		result.put(5, 2);
		
		Assertions.assertEquals(this.atmService.withdraw("01001", 100), result);
		Assertions.assertEquals(this.atmService.checkBalance("01001"), "GBP $ " + BigDecimal.valueOf(2538.59));
	}
	
	@Test
	@DisplayName("Test ATM Replenish")
	public void testReplenish() {
		
		Assertions.assertThrows(UnsopportedDenominationException.class, () -> this.atmService.replenish(6, 10));
	}
	
	private void replenshATM() {
		this.atmService.replenish(50, 50);
		this.atmService.replenish(20, 20);
		this.atmService.replenish(10, 10);
		this.atmService.replenish(5, 5);
	}
}
