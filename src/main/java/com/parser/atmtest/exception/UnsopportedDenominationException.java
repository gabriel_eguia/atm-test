/**
 * 
 */
package com.parser.atmtest.exception;

/**
 * @author gabriel
 *
 */
public class UnsopportedDenominationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2942498065568383371L;

	/**
	 * 
	 */
	public UnsopportedDenominationException(String message) {
		super(message);
	}

}
