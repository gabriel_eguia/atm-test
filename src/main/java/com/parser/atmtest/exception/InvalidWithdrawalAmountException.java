/**
 * 
 */
package com.parser.atmtest.exception;

/**
 * @author gabriel
 *
 */
public class InvalidWithdrawalAmountException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7988643846104013573L;

	public InvalidWithdrawalAmountException(String message) {
		super(message);
	}
}
