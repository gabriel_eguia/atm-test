/**
 * 
 */
package com.parser.atmtest.exception;

/**
 * @author gabriel
 *
 */
public class InsufficientFundsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7918243477831941839L;
	
	public InsufficientFundsException(String message) {
		super(message);
	}
}
