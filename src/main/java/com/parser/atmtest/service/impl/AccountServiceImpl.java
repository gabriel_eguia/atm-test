/**
 * 
 */
package com.parser.atmtest.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.parser.atmtest.entity.Account;
import com.parser.atmtest.exception.InsufficientFundsException;
import com.parser.atmtest.service.AccountService;

/**
 * @author gabriel
 *
 */
@Service
public class AccountServiceImpl implements AccountService {
	
	private HashMap<String, Account> accounts;
	private Logger logger;
	
	public AccountServiceImpl() {
		
		this.logger = LoggerFactory.getLogger(this.getClass());
		
		Account account;
		accounts = new HashMap<>();
		
		account = new Account("01001", BigDecimal.valueOf(2738.59));
		accounts.put(account.getAccountNumber(), account);
		account = new Account("01002", BigDecimal.valueOf(23.00));
		accounts.put(account.getAccountNumber(), account);
		account = new Account("01003", BigDecimal.valueOf(0.00));
		accounts.put(account.getAccountNumber(), account);
	}
	
	public BigDecimal checkBalance(String accountNumber) {
		return this.accounts.get(accountNumber).getBalance();
	}
	
	public BigDecimal withdraw(String accountNumber, BigDecimal amount) {
		
		Account account = this.accounts.get(accountNumber);
		
		this.logger.debug("Account No. " + account.getAccountNumber() + " - Balance GBP $ " + account.getBalance());
		
		if (amount.compareTo(account.getBalance()) > 0)
			throw new InsufficientFundsException("Insufficient funds for withdrawal");
		
		account.setBalance(account.getBalance().subtract(amount));
		
		return this.checkBalance(accountNumber);
	}
}
