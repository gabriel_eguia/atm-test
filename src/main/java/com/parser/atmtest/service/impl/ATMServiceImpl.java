/**
 * 
 */
package com.parser.atmtest.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parser.atmtest.exception.InsufficientFundsException;
import com.parser.atmtest.exception.InvalidWithdrawalAmountException;
import com.parser.atmtest.exception.UnsopportedDenominationException;
import com.parser.atmtest.service.ATMService;
import com.parser.atmtest.service.AccountService;

/**
 * @author gabriel
 *
 */
@Service
public class ATMServiceImpl implements ATMService {
	
	@Autowired
	private AccountService accountService;
	private Logger logger;
	
	private HashMap<Integer, Integer> notesAvailable;
	
	public ATMServiceImpl() {
		this.notesAvailable = new HashMap<Integer, Integer>();
		this.logger = LoggerFactory.getLogger(this.getClass());
	}
	
	public ATMServiceImpl(AccountService accountService) {
		this.notesAvailable = new HashMap<Integer, Integer>();
		this.accountService = accountService;
		this.logger = LoggerFactory.getLogger(this.getClass());
	}
	
	public String checkBalance(String accountNumber) {
		return "GBP $ " + this.accountService.checkBalance(accountNumber);
	}
	
	public HashMap<Integer, Integer> withdraw(String accountNumber, Integer amount) {
		
		if (amount < 20 || amount > 250)
			throw new InvalidWithdrawalAmountException("The withdrawal amount should be between 20 and 250 GBP");
		
		if (amount % 5 != 0)
			throw new InvalidWithdrawalAmountException("The withdrawal amount should be multiple of 5");
		
		if (amount > this.getAvailableAmount())
			throw new InvalidWithdrawalAmountException("There is not enough money in this ATM to fulfil the operation");
		
		if (this.accountService.checkBalance(accountNumber).compareTo(BigDecimal.valueOf(amount)) < 0)
			throw new InsufficientFundsException("Insufficient funds for withdrawal");
		
		this.logger.debug("Trying to withdraw GBP $ " + amount + " from Account No. " + accountNumber);
		this.logger.debug("Calculating which notes and amount of them should be delivered");
		
		int temp = amount;
		HashMap<Integer, Integer> notesToWithdraw = new HashMap<Integer, Integer>();
		List<Integer> availableNotesKeys = this.notesAvailable.keySet().stream().sorted((k1, k2) -> k2.compareTo(k1)).collect(Collectors.toList());
		
		if ((temp & 1) == 0 && this.notesAvailable.get(5) >= 2) {
			
			notesToWithdraw.put(5, 2);
			temp = temp - 10;
			this.notesAvailable.put(5, this.notesAvailable.get(5) - 2);
		}
		
		for (Integer key : availableNotesKeys) {
			
			int noteWithdrawalQantity = temp / key + (notesToWithdraw.containsKey(key) ? notesToWithdraw.get(key) : 0);
			
			if (noteWithdrawalQantity > 0 && this.notesAvailable.get(key) >= noteWithdrawalQantity) {
				
				notesToWithdraw.put(key, temp / key);
				temp = temp % key;
			}
		}
		
		if (temp != 0) {
			this.logger.error("The withdrawal cannot be fulfiled with the notes and amount combination given");
			throw new InvalidWithdrawalAmountException("This ATM cannot deliver that amount");
		}
		
		this.accountService.withdraw(accountNumber, BigDecimal.valueOf(amount));
		this.logger.debug("Successful withdrawal");
		
		return notesToWithdraw;
	}
	
	public int replenish(Integer denomination, Integer quantity) {
		
		this.logger.debug("Adding " + quantity + " notes of GBP $ " + denomination);
		
		if (denomination != 5L && denomination != 10L && denomination != 20L && denomination != 50L)
			throw new UnsopportedDenominationException("Unsopported denomination. Supported denominations are 5, 10, 20 and 50");
		
		int availableQuantity = 0;
		
		if (this.notesAvailable.containsKey(denomination))
			availableQuantity = this.notesAvailable.get(denomination);
		 
		this.notesAvailable.put(denomination, availableQuantity + quantity);
		
		return availableQuantity;
	}
	
	private int getAvailableAmount() {
		
		int availableAmount = 0;
		
		for (Entry<Integer, Integer> entry : this.notesAvailable.entrySet()) {
			availableAmount = availableAmount + (entry.getKey() * entry.getValue());
		}
		
		return availableAmount;
	}
}
