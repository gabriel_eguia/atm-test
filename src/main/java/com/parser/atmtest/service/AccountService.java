package com.parser.atmtest.service;

import java.math.BigDecimal;

public interface AccountService {
	
	public BigDecimal checkBalance(String accountNumber);
	
	public BigDecimal withdraw(String accountNumber, BigDecimal amount);
}
