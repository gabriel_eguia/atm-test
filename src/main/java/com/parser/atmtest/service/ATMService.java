/**
 * 
 */
package com.parser.atmtest.service;

import java.util.HashMap;

/**
 * @author gabriel
 *
 */
public interface ATMService {
	
	public String checkBalance(String accountNumber);
	
	public HashMap<Integer, Integer> withdraw(String accountNumber, Integer amount);
	
	public int replenish(Integer denomination, Integer quantity);
}
