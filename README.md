# ATM Test - Parser

Dependencies
- Java 8
- Spring 5.1.7
- JUnit 5.4.2

Assumptions
- The ATMs withdraw method should return a map indicating which notes and how many of them should be delivered to the customer